import { useEffect, useState, useContext, Fragment } from 'react';
import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function AdminProductView() {

    const { user } = useContext(UserContext);
    const history = useNavigate();
    const { productId } = useParams();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0);
    const [quantity, setQuantity] = useState(0);
    const [isActive, setIsActive] = useState("");
    const [image, setImage]= useState("");

    const updateProduct = (e) => {
        e.preventDefault();

        fetch(`https://quiet-reef-27529.herokuapp.com/products/${productId}`, {
            method : 'PUT',
            headers : {
                "Content-Type" : "application/json",
                Authorization : `Bearer ${ localStorage.getItem('token') }`
            },
            body : JSON.stringify({
                name : name,
                description : description,
                price : price,
                quantity : quantity,
                image : image,
                isActive : isActive
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data === true){
                Swal.fire({
                    title : "Successfully updated",
                    icon : "success",
                    text : `Product successfully updated`
                })
                history("/adminpage");
            }
            else{
                Swal.fire({
                    title : "Something went wrong",
                    icon : "error",
                    text : "Please try again."
                })
            }
        })
    }

    useEffect(() => {
        console.log(productId);

        fetch(`https://quiet-reef-27529.herokuapp.com/products/${productId}`)
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
            setQuantity(data.quantity);
            setIsActive(data.isActive);
            setImage(data.image);
        })

    }, [productId]);

	return(
        <Container fluid>
        <Row className='row-form m-0'>
            <Col className='col-form' md={{ span: 4, offset: 4 }} >
                <Form onSubmit={(e) => updateProduct(e)}>
                    <h1>Update Product</h1>
                    <Form.Group className="mb-3">
                        <Form.Label>Name</Form.Label>
                        <Form.Control
                            className='form'
                            type="String"
                            placeholder="Enter product name" 
                            value={name}
                            onChange={e => setName(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Description</Form.Label>
                        <Form.Control 
                            className='form'
                            type="string"
                            placeholder="Enter description" 
                            value={description}
                            onChange={e => setDescription(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Price</Form.Label>
                        <Form.Control 
                            className='form'
                            type="number"
                            placeholder="Enter price" 
                            value={price}
                            onChange={e => setPrice(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Quantity</Form.Label>
                        <Form.Control 
                            className='form'
                            type="number"
                            placeholder="Enter quantity" 
                            value={quantity}
                            onChange={e => setQuantity(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Image URL</Form.Label>
                        <Form.Control 
                            className='form'
                            type="string"
                            placeholder="Enter image URL" 
                            value={image}
                            onChange={e => setImage(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Product Availability: {String(isActive)}</Form.Label>
                        <div>
                            <Button className='m-2' variant="dark" onClick={e => setIsActive(true)}>True</Button>
                            <Button className='m-2' variant="dark" onClick={e => setIsActive(false)}>False</Button>
                        </div>
                    </Form.Group>

                    <Button variant="dark" type="submit" id="submitBtn">
                        Update
                    </Button>

                </Form>
            </Col>
        </Row>
        </Container>
    )
}