import { Row, Col, Container, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function AdminText() {
    return(
        <Container fluid>
            <Row className=' m-0'>
                <div className='adminPageTitle'>Welcome to Admin Page</div>
                <Col className='homeAllText' md={{ span: 8, offset: 2 }} >
                    <Button variant="dark" className='adminButton' as={Link} to='/adminaddproduct' exact>Create New product</Button>
                    <Button variant="dark" className='adminButton' as={Link} to='/adminorderhistory' exact>Retrieve all orders</Button>
                </Col>
            </Row>
        </Container>
    )
}


