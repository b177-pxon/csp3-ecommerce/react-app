import { useEffect, useState, useContext, Fragment } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {

    const { user } = useContext(UserContext);
    const history = useNavigate();
    const { productId } = useParams();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0);
    const [quantity, setQuantity] = useState(0);
    const [image, setImage] = useState("");
    
    const [count, setCount] = useState(1);  
    const incrementCount = () => {  
        setCount(count + 1);  
    };  
    const decrementCount = () => {  
        setCount((c) => Math.max(c - 1, 1));  
    };

    const numberFormat = (value) =>
    new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'PHP'
    }).format(value);

    const order = (productId) => {
        fetch('https://quiet-reef-27529.herokuapp.com/users/checkout', {
            method : 'POST',
            headers : {
                "Content-Type" : "application/json",
                Authorization : `Bearer ${ localStorage.getItem('token') }`
            },
            body : JSON.stringify({
                productId : productId,
                quantity : count
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data === true){
                Swal.fire({
                    title : "Successfully bought",
                    icon : "success",
                    text : `You have successfully bought ${count} pc/s of this product.`
                })
                history("/orderhistory");
            }
            else{
                Swal.fire({
                    title : "Something went wrong",
                    icon : "error",
                    text : "Please try again."
                })
            }
        })
    }

    useEffect(() => {
        console.log(productId);

        fetch(`https://quiet-reef-27529.herokuapp.com/products/${productId}`)
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
            setQuantity(data.quantity);
            setImage(data.image);
        })

    }, [productId]);

	return(
		<Container fluid className='mt-5 pt-5'>

            <Card>
                <Row>
                    <Col md={6}>
                        <div className='imageContainer'>
                            <img className='imageLarge' src={`${image}`}></img>
                        </div>
                    </Col>

                    <Col md={6} className='productViewContent'>
                        <Card.Body>
                        <Card.Title>{name}</Card.Title>
                        <Card.Subtitle className="mb-2 text-muted">Description: {description}</Card.Subtitle>
                        <Card.Subtitle className="mb-2 text-muted">Price: {numberFormat(price)}</Card.Subtitle>
                        <Card.Subtitle className="mb-2 text-muted">Stock: {quantity}</Card.Subtitle>
                        <div>
                            <button className='btn btn-dark btn-block' type="button" onClick={decrementCount} > - </button>  
                            <span className='m-5'>{count} pc/s</span> 
                            <button className='btn btn-dark btn-block' type="button" onClick={incrementCount} > + </button>
                        
                        { user.id !== null ?
                            <Button variant="dark" className='adminButton' onClick={() => order(productId)} block>Buy</Button>
                            :
                            <Fragment>
                                <Button variant="dark" className='adminButton' as={Link} to='/login' exact>Log in to Buy</Button>
                            </Fragment>
                        }
                        </div>
                        </Card.Body>
                    </Col>
                </Row>

            </Card>

		</Container>
	)
}
