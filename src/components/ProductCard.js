import PropTypes from 'prop-types';
import { Card, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}) {

    const {_id, name, price, image} = productProp;
    const numberFormat = (value) =>
    new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'PHP'
}).format(value);

    return (
        <Col md={3}>
            <Card className='productCard'>
            <Card.Body>   
                <img className='imageCollection' src={`${image}`}></img>
                <p className='cardTitle m-0'>{name}</p>
                <p className='cardProps'>{numberFormat(price)}</p>
                <Link className='btn btn-dark' to={`/collection/${_id}`}>View Product</Link>
            </Card.Body>
            </Card>
        </Col>
    )
}

ProductCard.propTypes = {
    productProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        quantity: PropTypes.number.isRequired
    })
}
