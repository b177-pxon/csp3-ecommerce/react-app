import { Card, Container, Col, Row } from 'react-bootstrap';

export default function AdminOrderCard({orderProp}) {

    const { productName, userFirstName, userLastName, quantity, totalAmount, purchasedOn, image } = orderProp;
    const numberFormat = (value) =>
    new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'PHP'
    }).format(value);

    return (
        <Container fluid>
            <Card className='card'>
            <Card.Body>
                <Row className='justify-content-center align-items-center'>
                    <Col md={1}>
                        <img className='imageSmall' src={`${image}`}></img>
                    </Col>
                    <Col md={2}>
                        <p className='cardTitle m-0'>{productName}</p>
                    </Col>
                    <Col>
                        <div className='my-1'>
                            <span className='cardText'>User: </span>
                            <span className='cardProps'>{userFirstName} {userLastName}</span>
                        </div>
                        <div className='my-1'>
                            <span className='cardText'>Price: </span>
                            <span className='cardProps'>{numberFormat(totalAmount)}</span>
                        </div>
                        <div className='my-1'>
                            <span className='cardText'>Qty: </span>
                            <span className='cardProps'>{quantity} pc/s</span>
                        </div>
                        <div className='my-1'>
                            <span className='cardText'>Date </span>
                            <span className='cardProps'>{purchasedOn}</span>
                        </div>
                    </Col>
                </Row>
            </Card.Body>
            </Card>
        </Container>
    )

}