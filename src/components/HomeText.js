import { Container, Row, Col } from 'react-bootstrap';

export default function HomeText() {
    return(
        <Container fluid>
            <Row className='row-form m-0'>
                <Col className='homeAllText p-0' md={{ span: 6, offset: 3 }} >
                    <Row className='m-5'>
                        <span className='homeTitle p-0'>JShop by JShone</span>
                    </Row>
                    <Row className='m-5'>
                        <span className='homeText p-0'>JShop by JShone is an AUTHORIZED DISTRIBUTOR of Seiko Products in the Philippines.</span>
                    </Row>
                </Col>
            </Row>
        </Container>
    )
}