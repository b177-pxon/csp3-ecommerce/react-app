import React, { useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import { Container } from 'react-bootstrap';

export default function AppNavbar() {
    const { user } = useContext(UserContext);

    return (
        <Container fluid>
            <Navbar expand="lg" className='fixed-top m-5 p-0'>
            <Navbar.Brand className='ps-3' as={Link} to='/'>JShop by JShone</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                    <Nav.Link className='ps-3 navbarText' as={Link} to='/collection'>Collection</Nav.Link>
                {(user.isAdmin) ?
                    <Nav.Link className='ps-3 navbarText' as={Link} to='/adminpage' exact>Admin page</Nav.Link>
                :
                    null
                }
                {(!user.isAdmin && user.id !== null) ?
                    <Nav.Link className='ps-3 navbarText' as={Link} to='/orderHistory' exact>Orders</Nav.Link>
                :
                    null
                }
                </Nav>
                {((user.id !== null) && (!user.isAdmin && user.id !== null) ) ?
                    <Nav className='ps-3 pe-3 navbarUser'>
                        Happy Shopping {user.firstName} !
                    </Nav>
                        :
                null}
                <Nav>
                    {(user.id !== null) ?
                        <React.Fragment>
                            <Nav.Link className='ps-3 navbarText' as={Link} to='/logout' exact>Logout</Nav.Link>
                        </React.Fragment>
                        :
                        <React.Fragment>
                            <Nav.Link className='ps-3 navbarText' as={Link} to='/login'>Login</Nav.Link>
                        </React.Fragment>
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
        </Container>
        
    )
}



