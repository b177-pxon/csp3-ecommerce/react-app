import { useEffect, useState, useContext } from "react";
import AdminProductCard from "./AdminProductCard";
import UserContext from '../UserContext';
import { Container } from 'react-bootstrap';

export default function AdminCollection() {

    const { user } = useContext(UserContext);
    const [products, setProducts] = useState([]);

    useEffect(() => {
        fetch('https://quiet-reef-27529.herokuapp.com/products/all', {
            method : 'GET',
            headers : {
                "Content-Type" : "application/json",
                Authorization : `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setProducts(data.map(products => {
                return (
                    <AdminProductCard key={products._id} productProp={products} />
                );
            }))
        })
    }, []);

    return(
        <Container fluid>
            <p className="pageTitle">All Products</p>
            {products}
        </Container>
    )
}