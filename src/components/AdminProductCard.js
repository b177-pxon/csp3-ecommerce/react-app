import PropTypes from 'prop-types';
import { Card, Container, Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function AdminProductCard({productProp}) {

    const {_id, name, description, price, quantity, image, isActive} = productProp;
    const numberFormat = (value) =>
        new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'PHP'
    }).format(value);

    return (
        <Container fluid>
            <Card className='card'>
            <Card.Body>
                <Row className='justify-content-center align-items-center'>
                    <Col md={1}>
                        <img className='imageSmall' src={`${image}`}></img>
                    </Col>
                    <Col md={1}>
                        <Link className='btn btn-dark' to={`/admincollection/${_id}`}>Update Product</Link>
                    </Col>
                    <Col md={2}>
                        <p className='cardTitle m-0'>{name}</p>
                    </Col>
                    <Col>
                        <div className='my-1'>
                            <span className='cardText'>Description: </span>
                            <span className='cardProps'>{description}</span>
                        </div>
                        <div className='my-1'>
                            <span className='cardText'>Price: </span>
                            <span className='cardProps'>{numberFormat(price)}</span>
                        </div>
                        <div className='my-1'>
                            <span className='cardText'>Qty: </span>
                            <span className='cardProps'>{quantity} pc/s</span>
                        </div>
                        <div className='my-1'>
                            <span className='cardText'>Product Availability: </span>
                            <span className='cardProps'>{String(isActive)}</span>
                        </div>
                    </Col>
                </Row>
            </Card.Body>
            </Card>
        </Container>
    )
}

AdminProductCard.propTypes = {
    productProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        quantity: PropTypes.number.isRequired,
        isActive: PropTypes.bool.isRequired
    })
}
