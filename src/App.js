import AppNavbar from './components/AppNavbar';
import { Fragment, useEffect, useState } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import './App.css';
import { Route, Routes } from 'react-router-dom';
import Collection from './pages/Collection';
import AdminAddProduct from './pages/AdminAddProduct';
import AdminProductView from './components/AdminProductView';
import OrderHistory from './pages/OrderHistory';
import AdminOrderHistory from './pages/AdminOrderHistory';
import Home from './pages/Home';
import Register from './pages/Register';
import AdminPage from './pages/AdminPage';
import Login from './pages/Login';
import Logout from './pages/Logout'
import PageNotFound from './pages/PageNotFound';
import { UserProvider } from './UserContext';
import ProductView from './components/ProductView';

function App() {

  const [user, setUser] = useState({
    id : null,
    isAdmin : null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container fluid className='bg-color'>
          <Routes>
            <Route path='/' element={<Home />} />
            <Route path='/collection' element={<Collection />} />
            <Route path='/collection/:productId' element={<ProductView />} />
            <Route path='/admincollection/:productId' element={<AdminProductView />} />
            <Route path='/adminaddproduct' element={<AdminAddProduct />} />
            <Route path='/orderhistory' element={<OrderHistory />} />
            <Route path='/adminorderhistory' element={<AdminOrderHistory />} />
            <Route path='/adminpage' element={<AdminPage />} />
            <Route path='/login' element={<Login />} />
            <Route path='/logout' element={<Logout />} />
            <Route path='/register' element={<Register />} />
            <Route path='*' element={<PageNotFound />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
