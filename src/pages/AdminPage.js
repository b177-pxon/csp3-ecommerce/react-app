import { Container } from 'react-bootstrap';
import AdminText from '../components/AdminText';
import AdminCollection from '../components/AdminCollection';

export default function AdminPage(){
    return(
        <Container fluid className='m-0 p-0 mt-5 pt-5'>
            <AdminText />
            <AdminCollection />
        </Container>
        )
    }

