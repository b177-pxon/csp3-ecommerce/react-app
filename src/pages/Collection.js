import { useEffect, useState, useContext } from "react";
import ProductCard from "../components/ProductCard";
import UserContext from '../UserContext';
import { Container, Row } from 'react-bootstrap';

export default function Collection() {

    const { user } = useContext(UserContext);
    const [products, setProducts] = useState([]);

    useEffect(() => {
        fetch('https://quiet-reef-27529.herokuapp.com/products/')
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setProducts(data.map(products => {
                return (
                    <ProductCard key={products._id} productProp={products} />
                );
            }))
        })
    }, []);

    return(
        <Container fluid className="mt-5 pt-5">
            <p className="pageTitle">Seiko Collections</p>
            <Row className="collections m-0 p-0">
                {products}
            </Row>
        </Container>
    )
}