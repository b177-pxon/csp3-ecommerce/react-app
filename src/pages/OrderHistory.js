import { useEffect, useState, useContext } from "react";
import OrderCard from "../components/OrderCard";
import UserContext from '../UserContext';
import { Container } from 'react-bootstrap';

export default function OrderHistory() {

    const { user } = useContext(UserContext);
    const [orders, setOrders] = useState([]);

    useEffect(() => {

        fetch('https://quiet-reef-27529.herokuapp.com/users/myOrders', {
            method : 'GET',
            headers : {
                "Content-Type" : "application/json",
                Authorization : `Bearer ${ localStorage.getItem('token') }`
            }
        })

        .then(res => res.json())
        .then(data => {
            console.log(data);
            setOrders(data.map(order => {
                return (
                    <OrderCard key={order._id} orderProp={order} />
                );
            }))
        })
    }, []);

    return(
        <Container fluid className="mt-5 pt-5">
            <p className="pageTitle">{user.firstName}'s Order History</p>
            {orders}
        </Container>
    )
}