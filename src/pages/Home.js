import HomeText from '../components/HomeText';
import { Container } from 'react-bootstrap';

export default function Home() {
    return (
        <Container fluid className='m-0 p-0 row-form '>
            <HomeText />
        </Container>
    )
}