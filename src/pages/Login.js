import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login(){

    const { user, setUser } = useContext(UserContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    console.log(email);
    console.log(password);

    function login(e){
       e.preventDefault();

       fetch('https://quiet-reef-27529.herokuapp.com/users/login',{
           method: 'POST',
           headers: {
               'Content-Type': 'application/json'
           },
           body: JSON.stringify({
               email: email,
               password: password
           })
       })
       .then(res => res.json())
       .then(data => {
           console.log(data)

           if(typeof data.access !== "undefined"){
               localStorage.setItem('token', data.access);
               retrieveUserDetails(data.access);
            
               Swal.fire({
                   title: "Login Successful",
                   icon: "success",
                   text: "Welcome!"
               })
           }
           else{
               Swal.fire({
                   title: "Authentication failed",
                   icon: "error",
                   text: "Check your login details and try again."
               })
           }
       })
        setEmail('');
        setPassword('');
    }

    const retrieveUserDetails = (token) => {
        fetch('https://quiet-reef-27529.herokuapp.com/users/details', {
            headers : {
                Authorization : `Bearer ${ token }`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setUser({
                id : data._id,
                isAdmin : data.isAdmin,
                firstName : data.firstName
            })
            localStorage.setItem('id', data._id);
            localStorage.setItem('isAdmin', data.isAdmin);
        })
    }

    useEffect(() => {
        if(email !== '' && password !== '') {
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [email, password]);

    return(
        (user.id !== null) ?
        <Navigate to='/' />
        :
        <Row className='row-form m-0'>
            <Col className='col-form' md={{ span: 4, offset: 4 }} >

                <Form onSubmit={(e) => login(e)}>
                    <h1>LOGIN</h1>
                    <h6 className='my-3'>Please enter user details</h6>
                    <Form.Group className="mb-3" controlId="userEmail">
                        <Form.Control 
                            className='form'
                            type="email"
                            placeholder="Email" 
                            value={email}
                            onChange={e => setEmail(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="password">
                        <Form.Control 
                        className='form'
                        type="password" 
                        placeholder="Password" 
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        required
                        />
                    </Form.Group>

                    { isActive ?
                        <Button variant="dark" type="submit" id="submitBtn">
                        Login
                        </Button>
                    :
                        <Button variant="dark" type="submit" id="submitBtn" disabled>
                        Login
                        </Button>
                    }

                    <p className='mt-5'>Don't have an Account? <a href='/register'>Sign up</a></p>
                    
                </Form>
            </Col>
        </Row>

    )
}


