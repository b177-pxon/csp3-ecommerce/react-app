import { useEffect, useState, useContext } from "react";
import AdminOrderCard from "../components/AdminOrderCard";
import UserContext from '../UserContext';
import { Container } from 'react-bootstrap';

export default function AdminOrderHistory() {

    const { user } = useContext(UserContext);
    const [orders, setOrders] = useState([]);

    useEffect(() => {

        fetch('https://quiet-reef-27529.herokuapp.com/users/orders', {
            method : 'GET',
            headers : {
                Authorization : `Bearer ${ localStorage.getItem('token') }`
            }
        })

        .then(res => res.json())
        .then(data => {
            console.log(data);
            setOrders(data.map(order => {
                return (
                    <AdminOrderCard key={order._id} orderProp={order} />
                );
            }))
        })
    }, []);

    return(
        <Container fluid className="mt-5 pt-5">
            <p className="pageTitle">All Order History</p>
            {orders}
        </Container>
    )
}