import React from 'react';
import { Row, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function PageNotFound(){
    return(
        <Container fluid className='mt-5 pt-5'>
            <Row className='m-0 col-form'>
                <h1>Page Not Found</h1>
                <p>Go back to the <Link as={Link} to='/' exact>homepage.</Link></p>
            </Row>
        </Container>
    )
}