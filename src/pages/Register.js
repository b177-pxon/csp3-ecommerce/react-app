import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register(){
    
    const { user } = useContext(UserContext);
    const history = useNavigate();
    const [email, setEmail] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [isActive, setIsActive] = useState(false);

    function registerUser(e){
        e.preventDefault();

        setFirstName('');
        setLastName('');
        setEmail('');
        setPassword2('');
    }

    useEffect(() => {
        if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [email, firstName, lastName, password1, password2]);

    const register = (email) => {
        fetch('https://quiet-reef-27529.herokuapp.com/users/checkEmail/', {
            method : 'POST',
            headers : {
                "Content-Type" : "application/json",
            },
            body : JSON.stringify({
                email : email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data === false){
                const reg = () => {
                    fetch('https://quiet-reef-27529.herokuapp.com/users/register', {
                        method : 'POST',
                        headers : {
                            'Content-Type' : 'application/json'
                        },
                        body : JSON.stringify({
                            email : email,
                            firstName : firstName,
                            lastName : lastName,
                            password : password1
                        })
                    })
                    .then(res => res.json())
                    .then(data => {
                        console.log(data);
                    })
                }

                reg();

                Swal.fire({
                    title : "Registration successful",
                    icon : "success",
                    text : "Welcome!"
                })
                
                history("/login");
            }
            else{
                Swal.fire({
                    title : "Email already exists.",
                    icon : "error",
                    text : "Please register a different email."
                })
            }
        })
    }

    return(
		(user.id !== null) ?
			<Navigate to="/" />
		:
        <Row className='row-form m-0'>
            <Col className='col-form' md={{ span: 4, offset: 4 }} >
                <Form onSubmit={(e) => registerUser(e)}>
                    <h1>REGISTER</h1>
                    <h6 className='my-3'>Please enter user details</h6>
                    <Form.Group className="mb-3" controlId="userEmail">
                        <Form.Control
                            className='form'
                            type="email"
                            placeholder="Email" 
                            value={email}
                            onChange={e => setEmail(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="userFirstName">
                        <Form.Control 
                            className='form'
                            type="string"
                            placeholder="First name" 
                            value={firstName}
                            onChange={e => setFirstName(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="userLastName">
                        <Form.Control 
                            className='form'
                            type="string"
                            placeholder="Last name" 
                            value={lastName}
                            onChange={e => setLastName(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="password1">
                        <Form.Control 
                            className='form'
                            type="password" 
                            placeholder="Password" 
                            value={password1}
                            onChange={e => setPassword1(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="password2">
                        <Form.Control 
                            className='form'
                            type="password" 
                            placeholder="Verify Password" 
                            value={password2}
                            onChange={e => setPassword2(e.target.value)}
                            required
                        />
                    </Form.Group>

                    { isActive ?
                        <Button variant="primary" type="submit" id="submitBtn" onClick={() => register(email)}>
                        Submit
                        </Button>
                    :
                        <Button variant="dark" type="submit" id="submitBtn" disabled>
                        Submit
                        </Button>
                    }

                </Form>
            </Col>
        </Row>
    )
}

