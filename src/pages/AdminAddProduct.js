import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function AdminAddProduct(){
    
    const { user } = useContext(UserContext);
    const history = useNavigate();
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const [quantity, setQuantity] = useState('');
    const [isActive, setIsActive] = useState(false);
    const [image, setImage]= useState("");

    function registerProduct(e) {
        e.preventDefault();
        
        fetch('https://quiet-reef-27529.herokuapp.com/products/add', {
                method : 'POST',
                headers : {
                    'Content-Type' : 'application/json',
                    Authorization : `Bearer ${ localStorage.getItem('token') }`
                },
                body : JSON.stringify({
                    name : name,
                    description : description,
                    price : price,
                    image : image,
                    quantity : quantity
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if(data === true){
                    return(
                        Swal.fire({
                            title : "Product Registration successful",
                            icon : "success"
                        })
                    );
                }
                else {
                    return(
                        Swal.fire({
                            title : "Something went wrong",
                            icon : "error",
                            text : "Please try again later."
                        })
                    )

                }
            })

        setName('');
        setDescription('');
        setPrice('');
        setQuantity('');
        setImage('');
        
        history("/adminpage");
    }

    useEffect(() => {
        if(name !== '' && description !== '' && price !== '' && quantity !== '' && image !== ''){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [name, description, price, quantity]);

    return(
        <Row className='row-form m-0'>
            <Col className='col-form' md={{ span: 4, offset: 4 }} >
                <Form onSubmit={(e) => registerProduct(e)}>
                    <h1>Add Product</h1>
                    <Form.Group className="mb-3">
                        <Form.Label>Name</Form.Label>
                        <Form.Control
                            className='form'
                            type="String"
                            placeholder="Enter product name" 
                            value={name}
                            onChange={e => setName(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Description</Form.Label>
                        <Form.Control 
                            className='form'
                            type="string"
                            placeholder="Enter description" 
                            value={description}
                            onChange={e => setDescription(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Price</Form.Label>
                        <Form.Control 
                            className='form'
                            type="number"
                            placeholder="Enter price" 
                            value={price}
                            onChange={e => setPrice(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Quantity</Form.Label>
                        <Form.Control 
                            className='form'
                            type="number"
                            placeholder="Enter quantity" 
                            value={quantity}
                            onChange={e => setQuantity(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Image URL</Form.Label>
                        <Form.Control 
                            className='form'
                            type="string"
                            placeholder="Enter image URL" 
                            value={image}
                            onChange={e => setImage(e.target.value)}
                            required
                        />
                    </Form.Group>

                    { isActive ?
                        <Button variant="dark" type="submit" id="submitBtn">
                        Submit
                        </Button>
                    :
                        <Button variant="dark" type="submit" id="submitBtn" disabled>
                        Submit
                        </Button>
                    }

                </Form>
            </Col>
        </Row>
    )
}